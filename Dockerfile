# Stage 0, based on Bode.js, to bi=uild and compile Angular
FROM node:carbon as node

WORKDIR /app

COPY package*.json /app/

RUN npm install

COPY ./ /app/

ARG TARGET=ng-deploy-dev

RUN npm run ${TARGET}

#Stage 1, baased on Nginx, to have only the compiled app, ready for production with Nignx
FROM nginx:1.13

COPY --from=node /app/dist/ /usr/share/nginx/html

COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf

EXPOSE 80